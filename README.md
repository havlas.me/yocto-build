# Usage

```sh
podman run -it --name=ycbuild -v <DATADIR>:/data:z --userns=keep-id --hostuser=<USERNAME> yocto-build:bullseye
```

# Build
```sh
make yocto-build-bullseye
```
