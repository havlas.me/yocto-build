PODMAN ?= @podman
IMAGE_BASENAME ?= yocto-build

.PHONY: yocto-build-bullseye
yocto-build-bullseye:
	$(PODMAN) build -t $(IMAGE_BASENAME):bullseye bullseye
